import React from 'react';

const ModalInner = props => {
    return (
            <button className= "btn"
                onClick={props.closed}
            >X</button>
    );
};

export default ModalInner;