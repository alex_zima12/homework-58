import React from 'react';
import './Modal.css';
import Backdrop from "./Backdrop/Backdrop.js";

const Modal = props => {
    return (
        <>
            <Backdrop show = {props.show} clicked={props.closed}/>
        <div className="Modal"
             style={{
                 transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                 opacity: props.show ? '1' : '0'
             }}>
            <h3>{props.title}</h3>
            {props.children}
        </div>
        </>
    );
};

export default Modal;