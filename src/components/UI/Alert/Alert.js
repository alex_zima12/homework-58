import React from 'react';

const Alert = props => {

    const isClosed = props.dismiss;
    
    return (
        <div onClick={isClosed && props.clickDismissable?props.dismiss:""} className={['Alert', props.alertType].join(' ')} >
            {props.children}
            <button style={{ display: isClosed && !props.clickDismissable ? "block" : "none" }}
                    onClick={props.dismiss}
            >X</button>
        </div>
    );
    
   

};

export default Alert;