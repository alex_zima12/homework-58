import React, {useState} from 'react';
import "./App.css"
import Modal from "./components/UI/Modal/Modal.js"
import ModalInner from "./components/UI/Modal/ModalInner/ModalInner.js"
import Alert from "./components/UI/Alert/Alert.js"

function App() {
    const [purchasing, setPurchasing] = useState(false);
    const purchaseCanselHandler = () => {
        setPurchasing(false);
    };
    const purchaseCHandler = () => {
        setPurchasing(true);
    };

    const [dismissing, setDismissing] = useState(false);
    const dismissHandler = () => {
        setDismissing(false);
    };

    const continued = () =>{
        alert("324324324");
    }

    const closed = () =>{
        purchaseCanselHandler();
    }

    const buttons=[
        {type: 'primary', label: 'Continue', clicked: continued},
        {type: 'danger', label: 'Close', clicked: closed}
        ];        
    return (
        <div className='App'>
            <Modal
                show={purchasing}
                closed={purchaseCanselHandler}
                title="Some kinda modal title">
                <ModalInner
                    closed={purchaseCanselHandler}                    
                />
                <p>This is modal content</p>
                {buttons.map(el=>{ return (<button className={el.type} onClick={el.clicked}>{el.label}</button>);})}
            </Modal>
            <button
                onClick={purchaseCHandler}
            > Click me
            </button>
            <div className="alertBlock">
                <Alert
                    alertType="Success"
                    dismiss={continued}
                    clickDismissable={true}
                >This is a success type alert </Alert>
            </div>
            <div className="alertBlock">
                <Alert
                    alertType="Success"
                    dismiss={continued}
                    clickDismissable={false}
                >This is a success type alert </Alert>
            </div>
            <div className="alertBlock">
                <Alert
                    alertType="Primary"
                >This is a primary type alert </Alert>
            </div>
        </div>
    );
};

export default App;
